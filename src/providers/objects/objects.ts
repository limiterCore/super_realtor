import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Storage} from "@ionic/storage";

// Providers
import {AuthProvider, UserInterface} from "../auth/auth";
import {DbProvider} from "../db/db";

interface ObjectInterfaceWrap {
	data: ObjectInterface[],
}

export interface ObjectInterface {
	id: number;
	name: string;
	description: string;
	price: number;
	date: string;
	images: string[];
	phones: string[];
	in_favourites?: number;
	is_viewed?: number;
}

export interface FiltersInterface {
	city_id: number;
	city_area: number | null;
	price_from: number;
	price_to: number;
}

// export interface ImageInterface {
//   id: number;
//   path: string;
//   object_id: number;
//   position: number;
// }
//
// export interface PhoneInterface {
//   id: number;
//   phone: string;
//   object_id: number;
//   position: number;
// }

@Injectable()
export class ObjectsProvider {

	apiUrl = 'http://push-soft.com/sr/public';

	filters: FiltersInterface;


	private newObjectsCountSource = new BehaviorSubject<number>(0);
	newObjectsCount = this.newObjectsCountSource.asObservable();

	private objectsSource = new BehaviorSubject<ObjectInterface[]>([]);
	objects = this.objectsSource.asObservable();

	private objectsFavSource = new BehaviorSubject<ObjectInterface[]>([]);
	objectsFav = this.objectsFavSource.asObservable();

	constructor(public http: HttpClient,
	            public authProvider: AuthProvider,
	            public storage: Storage,
	            public dbProvider: DbProvider,) {
	}

	/**
	 * Get new objects which fit for filters
	 * @returns Promise
	 */
	getObjects() {
		return new Promise((resolve, reject) => {
			// If objects exist - return them
			let currentObjects = this.getCurrentObjects();
			if (currentObjects.length) {
				return resolve();
			}
			else {
				// Try to get from database
				this.dbProvider.getObjects()
					.then((objects: ObjectInterface[]) => {
						this.objectsSource.next([...objects]);
						return resolve();
					})
					.catch(err => {
						// Get from API
						this.authProvider.getUser()
							.then((user: UserInterface) => {
								this.http.get(this.apiUrl + '/get', {
									headers: new HttpHeaders()
										.set('Content-Type', 'application/json')
										.set('Authorization', 'Bearer ' + user.token)
								})
									.subscribe((data: ObjectInterfaceWrap) => {


										// Add to database
										this.dbProvider.insertObjects(data.data)
											.then(() => {
												return this.dbProvider.getObjects();
											})
											.then((objects: ObjectInterface[]) => {
												this.objectsSource.next([...objects]);
												this.changeNewObjectsCount(data.data.length, 'plus');
												resolve();
											});
									}, err => {
										reject(err);
									});
							});
					});
			}
		});
	}

	/**
	 * Get new objects from API
	 * @returns {Promise<any>}
	 */
	getObjectsFromAPI() {
		return new Promise((resolve, reject) => {
			this.authProvider.getUser()
				.then((user: UserInterface) => {
					this.http
						.get(this.apiUrl + '/get', {
								headers: new HttpHeaders()
									.set('Content-Type', 'application/json')
									.set('Authorization', 'Bearer ' + user.token)
							}
						)
						.subscribe((data: ObjectInterfaceWrap) => {
							resolve(data.data);
						});
				}, err => {
					reject(err);
				});
		});
	}

	/**
	 * Get more objects from db
	 * @returns Promise
	 */
	getMoreObjects(setNumber: number) {
		return new Promise((resolve, reject) => {

			// Try to get from database
			this.dbProvider.getObjects(setNumber)
				.then((objects: ObjectInterface[]) => {
					let currentObjects = this.getCurrentObjects();
					this.objectsSource.next([...currentObjects, ...objects]);
					return resolve();
				})
				.catch(err => {
					reject();
				});

		});
	}

	/**
	 * Get favourite objects
	 * @returns Promise
	 */
	getFavObjects() {
		return new Promise((resolve, reject) => {
			// If objects exist - return them
			let currentFavObjects = this.getCurrentFavObjects();
			if (currentFavObjects.length) {
				return resolve();
			}
			else {
				// Try to get from database
				this.dbProvider.getFavObjects()
					.then((objects: ObjectInterface[]) => {
						this.objectsFavSource.next([...objects]);
						return resolve();
					})
					.catch(error => {
						reject(error);
					});
			}
		});
	}

	/**
	 * Get more favourite objects from db
	 * @returns Promise
	 */
	getMoreFavObjects(setNumber: number) {
		return new Promise((resolve, reject) => {

			// Try to get from database
			this.dbProvider.getFavObjects(setNumber)
				.then((objects: ObjectInterface[]) => {
					let currentFavObjects = this.getCurrentFavObjects();
					this.objectsFavSource.next([...currentFavObjects, ...objects]);
					if (objects.length > 0) {
						resolve();
					}
					else {
						reject();
					}
				})
				.catch(err => {
					reject();
				});

		});
	}

	/**
	 * Set filters that affect getObjects return
	 * @param {FiltersInterface} filters
	 * @returns {Promise<any>}
	 */
	setFilters(filters: FiltersInterface) {
		return new Promise((resolve, reject) => {
			this.authProvider.getUser().then((user: UserInterface) => {
				this.http.post(this.apiUrl + '/filter', filters, {
					headers: new HttpHeaders()
						.set('Content-Type', 'application/json')
						.set('Authorization', 'Bearer ' + user.token)
				}).subscribe((data) => {
					this.filters = filters;
					this.storage.set('filters', this.filters).then(() => {
						resolve(data);
					});
				}, err => {
					reject(err);
				});
			});
		});
	}

	/**
	 *
	 * @returns {Promise}
	 */
	getCurrentFilters() {
		return new Promise((resolve, reject) => {
			if (this.filters) {
				return resolve(this.filters);
			}
			else {
				this.storage.get('filters').then((filters: FiltersInterface) => {
					if (filters) {
						this.filters = filters;
						resolve(this.filters);
					}
					else {
						reject(false);
					}
				});
			}
		});
	}

	/**
	 * Add object to favourites
	 * @param {number} id
	 * @returns {Promise<any>}
	 */
	addToFavourites(id: number) {
		return new Promise((resolve, reject) => {
			let objects = this.getCurrentObjects();
			let index = objects.findIndex(item => item.id === id);
			if (index > -1) {

				// Update field in database
				this.dbProvider.setInFavourites(id, 1).then(() => {
					// @todo: remove this (dev)
					console.log('Set favourite ' + id);

					objects[index].in_favourites = 1;

					// Update objects
					this.objectsSource.next(objects);

					// Update favourite objects
					this.objectsFavSource.next([objects[index], ...this.getCurrentFavObjects()]);

					resolve();
				}).catch(err => {
					reject(err);
				});

			}
			else {
				reject(false);
			}
		});
	}

	/**
	 * Remove object from favourites
	 * @param {number} id
	 * @returns {Promise<any>}
	 */
	removeFromFavourites(id: number) {
		return new Promise((resolve, reject) => {

			// Update field in database
			this.dbProvider.setInFavourites(id, 0).then(() => {
				// @todo: remove this (dev)
				console.log('Remove favourite ' + id);

				// Update objects
				let objects = this.getCurrentObjects();
				let index = objects.findIndex(item => item.id === id);
				if (index > -1) {
					objects[index].in_favourites = 0;
					this.objectsSource.next(objects);
				}

				// Update favourite objects
				let objectsFav = this.getCurrentFavObjects();
				let favIndex = objectsFav.findIndex(item => item.id === id);
				if (favIndex > -1) {
					objectsFav.splice(favIndex, 1);
					this.objectsFavSource.next(objectsFav);
				}

				resolve();
			}).catch(err => {
				reject(err);
			});
		});
	}

	/**
	 * Mark item as viewed
	 * @param {number} id
	 * @returns {Promise<any>}
	 */
	markAsViewed(id: number) {
		return new Promise((resolve, reject) => {
			let objects = this.getCurrentObjects();
			let index = objects.findIndex(item => item.id === id);
			if (index > -1) {
				// Update field in database
				this.dbProvider.markAsViewed(id)
					.then(() => {
						// @todo: remove this (dev)
						console.log('Set viewed');

						objects[index].is_viewed = 1;
						this.objectsSource.next(objects);
						resolve();
					})
					.catch(error => {
						reject(error);
					});
			}
			else {
				reject(`Item with id=${id} not found`);
			}
		});
	}

	/**
	 * Set new count for new objects
	 * @param {number} count
	 * @param {string} operation
	 */
	changeNewObjectsCount(count: number, operation?: string) {
		let currentNewObjectsCount = this.getNewObjectsCount();

		if (operation === 'plus') {
			this.newObjectsCountSource.next(currentNewObjectsCount + count);
		}
		else if (operation === 'minus') {
			if (currentNewObjectsCount - count >= 0) {
				this.newObjectsCountSource.next(currentNewObjectsCount - count);
			}
		}
		else {
			this.newObjectsCountSource.next(count);
		}
	}

	getNewObjectsCount() {
		let currentCount: number = 0;
		this.newObjectsCount.subscribe(count => currentCount = count);
		return currentCount;
	}

	/**
	 * Merge new objects with existing ones
	 * @param {ObjectInterface[]} objects
	 */
	addNewObjects(objects: ObjectInterface[]) {
		this.objectsSource.next([...objects, ...this.getCurrentObjects()]);
	}

	/**
	 * Get existing objects
	 * @returns {ObjectInterface[]}
	 */
	getCurrentObjects() {
		let currentObjects: ObjectInterface[] = [];
		this.objects.subscribe(objects => currentObjects = objects);
		return currentObjects;
	}

	/**
	 * Get existing favourite objects
	 * @returns {ObjectInterface[]}
	 */
	getCurrentFavObjects() {
		let currentFavObjects: ObjectInterface[] = [];
		this.objectsFav.subscribe(objects => currentFavObjects = objects);
		return currentFavObjects;
	}

	/**
	 * Clear objects, filters, favourites
	 * @returns {Promise<any>}
	 */
	clearProperties() {
		return new Promise(resolve => {
			this.objectsSource.next([]);
			this.objectsFavSource.next([]);

			this.filters = undefined;

			resolve();
		});

	}
}
