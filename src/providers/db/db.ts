import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';

// Providers and Interfaces
import {CityInterface, DistrictInterface} from "../areas/areas";
import {ObjectInterface} from "../objects/objects";

@Injectable()
export class DbProvider {

	db: SQLiteObject;
	objectsPerPage: number = 10;

	constructor(public http: HttpClient,
	            private sqlite: SQLite,) {
	}

	initTables() {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					this.db.sqlBatch([
						'CREATE TABLE IF NOT EXISTS `cities` (' +
						' `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
						' `name` TEXT NOT NULL,' +
						' `remote_id` INTEGER NOT NULL ' +
						') ',
						'CREATE TABLE IF NOT EXISTS `city_areas` ( ' +
						'`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' +
						'`name` TEXT NOT NULL, ' +
						'`remote_id` INTEGER NOT NULL, ' +
						'`city_id` INTEGER NOT NULL ' +
						')',
						'CREATE TABLE IF NOT EXISTS `objects` (' +
						'`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' +
						'`name` TEXT NOT NULL DEFAULT "", ' +
						'`description` TEXT NOT NULL DEFAULT "", ' +
						'`date` TEXT DEFAULT "", ' +
						'`city` TEXT DEFAULT "", ' +
						'`region` TEXT DEFAULT "", ' +
						'`price` INTEGER NOT NULL DEFAULT 0, ' +
						'`remote_id` INTEGER NOT NULL, ' +
						'`is_viewed` INTEGER NOT NULL, ' +
						'`in_favourites` INTEGER NOT NULL DEFAULT 0' +
						')',
						'CREATE TABLE IF NOT EXISTS `images` ( ' +
						'`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' +
						'`path` TEXT NOT NULL, ' +
						'`object_id` INTEGER NOT NULL, ' +
						'`position` INTEGER NOT NULL' +
						')',
						'CREATE TABLE IF NOT EXISTS `phones` ( ' +
						'`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' +
						'`phone` TEXT NOT NULL, ' +
						'`object_id` INTEGER NOT NULL, ' +
						'`position` INTEGER NOT NULL' +
						')'
					]).then(() => {
						// @todo: remove (dev)
						console.log('Database populated');

						resolve(true);
					}).catch(err => {
						// @todo: remove (dev)
						console.log('SQL batch ERROR for initTables: ' + err.message);

						reject(err.message);
					});
				})
				.catch(err => {
					reject(err.message);
				});
		});
	}

	clearTables() {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					return this.db.sqlBatch([
						'DROP TABLE IF EXISTS `cities`',
						'DROP TABLE IF EXISTS `city_areas`',
						'DROP TABLE IF EXISTS `objects`',
						'DROP TABLE IF EXISTS `images`',
						'DROP TABLE IF EXISTS `phones`'
					]);
				})
				.then(() => {
					// @todo: remove (dev)
					console.log('Tables dropped');

					resolve(true);
				})
				.catch(err => {
					// @todo: remove (dev)
					console.log(err.message);
					reject(err);
				});
		});
	}

	getDb() {
		return new Promise((resolve) => {
			if (this.db) {
				resolve(true);
			}
			else {
				this.sqlite.create({
					name: 'superrealtor.db',
					location: 'default',
				})
					.then((db: SQLiteObject) => {
						this.db = db;
						resolve(true);
					});
			}
		});
	}

	fillAreas(cities: CityInterface[], districts: DistrictInterface[]) {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					return this.getCities()
				})
				.then((existingCities: CityInterface[]) => {
					if (existingCities.length > 0) {
						resolve(true); // Already exist
					}
					else {
						let sqls = [];
						cities.forEach((item) => {
							sqls.push(['INSERT INTO cities(name, remote_id) VALUES (?,?)', [item.name, item.id]]);
						});
						districts.forEach(item => {
							sqls.push(['INSERT INTO city_areas(name, remote_id, city_id) VALUES (?, ?, ?)', [item.name, item.id, item.city_id]]);
						});

						this.db.sqlBatch(sqls).then(() => {
							// @todo: remove (dev)
							console.log('Rows inserted');

							resolve(true);
						}).catch(err => {
							// @todo: remove (dev)
							console.log('SQL batch ERROR for areas: ' + err.message);

							reject(err.message);
						});
					}
				});
		});
	}

	getCities() {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					this.db.executeSql('SELECT * FROM cities', []).then((resultSet) => {
						let cities: CityInterface[] = [];
						for (let x = 0; x < resultSet.rows.length; x++) {
							cities.push({
								name: resultSet.rows.item(x).name,
								id: resultSet.rows.item(x).remote_id,
							});
						}
						resolve(cities);
					}).catch(err => {
						reject(err.message);
					});
				});
		});
	}

	getDistricts() {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					this.db.executeSql('SELECT * FROM city_areas', []).then((resultSet) => {
						let districts: DistrictInterface[] = [];
						for (let x = 0; x < resultSet.rows.length; x++) {
							districts.push({
								name: resultSet.rows.item(x).name,
								id: resultSet.rows.item(x).remote_id,
								city_id: resultSet.rows.item(x).city_id,
							});
						}
						resolve(districts);
					}).catch(err => {
						reject(err.message);
					});
				});
		});
	}

	getObjects(setNumber?: number) {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					let sql: string = `SELECT * FROM objects ORDER BY id DESC LIMIT ${this.objectsPerPage}`;
					if (setNumber) {
						sql += ` OFFSET ${setNumber} * ${this.objectsPerPage}`;
					}
					return this.db.executeSql(sql, [])
				})
				.then((resultSet) => {
					let objects: ObjectInterface[] = [];

					(async function loop(db) {

						for (let x = 0; x < resultSet.rows.length; x++) {
							let imagesQuery = db.executeSql('SELECT * FROM images WHERE object_id = ? ORDER BY position ASC', [resultSet.rows.item(x).remote_id]);
							let phonesQuery = db.executeSql('SELECT * FROM phones WHERE object_id = ? ORDER BY position ASC', [resultSet.rows.item(x).remote_id]);

							await Promise.all([imagesQuery, phonesQuery]).then(values => {
								let images: string[] = [];
								let phones: string[] = [];
								for (let ix = 0; ix < values[0].rows.length; ix++) {
									images.push(values[0].rows.item(ix).path);
								}
								for (let px = 0; px < values[1].rows.length; px++) {
									phones.push(values[1].rows.item(px).phone);
								}

								objects.push({
									id: resultSet.rows.item(x).remote_id,
									name: resultSet.rows.item(x).name,
									description: resultSet.rows.item(x).description,
									price: resultSet.rows.item(x).price,
									date: resultSet.rows.item(x).date,
									images: images,
									phones: phones,
									in_favourites: resultSet.rows.item(x).in_favourites,
									is_viewed: resultSet.rows.item(x).is_viewed,
								});
							}).catch(err => {
								// @todo: remove this (dev)
								console.log(err);
							});
						}

						if (objects.length > 0) {
							resolve(objects);
						}
						else {
							reject('No objects in database');
						}

					})(this.db);

				}).catch(err => {
					reject(err.message);
				});
		});
	}

	getFavObjects(setNumber?: number) {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					let sql: string = `SELECT * FROM objects WHERE in_favourites = ? ORDER BY id DESC LIMIT ${this.objectsPerPage}`;
					if (setNumber) {
						sql += ` OFFSET ${setNumber} * ${this.objectsPerPage}`;
					}

					return this.db.executeSql(sql, [1]);
				})
				.then((resultSet) => {
					let objects: ObjectInterface[] = [];

					(async function loop(db) {

						for (let x = 0; x < resultSet.rows.length; x++) {
							let imagesQuery = db.executeSql('SELECT * FROM images WHERE object_id = ? ORDER BY position ASC', [resultSet.rows.item(x).remote_id]);
							let phonesQuery = db.executeSql('SELECT * FROM phones WHERE object_id = ? ORDER BY position ASC', [resultSet.rows.item(x).remote_id]);

							await Promise.all([imagesQuery, phonesQuery]).then(values => {
								let images: string[] = [];
								let phones: string[] = [];
								for (let ix = 0; ix < values[0].rows.length; ix++) {
									images.push(values[0].rows.item(ix).path);
								}
								for (let px = 0; px < values[1].rows.length; px++) {
									phones.push(values[1].rows.item(px).phone);
								}

								objects.push({
									id: resultSet.rows.item(x).remote_id,
									name: resultSet.rows.item(x).name,
									description: resultSet.rows.item(x).description,
									price: resultSet.rows.item(x).price,
									date: resultSet.rows.item(x).date,
									images: images,
									phones: phones,
									in_favourites: resultSet.rows.item(x).in_favourites,
									is_viewed: resultSet.rows.item(x).is_viewed,
								});
							}).catch(err => {
								// @todo: remove this (dev)
								console.log(err);
							});
						}

						resolve(objects);
					})(this.db);


				})
				.catch(err => {
					reject(err.message);
				});
		});
	}

	insertObjects(objects: ObjectInterface[]) {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					let sqls = [];
					objects.forEach((item) => {
						sqls.push(['INSERT or REPLACE into objects (id, name, description, price, date, remote_id, is_viewed) values' +
						'((select id from objects where remote_id = ?), ?, ?, ?, ?, ?, ?)', [item.id, item.name, item.description, item.price, item.date, item.id, 0]]);

						// Insert images
						let i = 0;
						item.images.forEach(image => {
							sqls.push(['INSERT or REPLACE into images (id, path, object_id, position) values' +
							'((select id from images where path = ? and object_id = ?), ?, ?, ?)', [image, item.id, image, item.id, i]]);
							i++;
						});

						// Insert phones
						i = 0;
						item.phones.forEach(phone => {
							sqls.push(['INSERT or REPLACE into phones (id, phone, object_id, position) values' +
							'((select id from phones where phone = ? and object_id = ?), ?, ?, ?)', [phone, item.id, phone, item.id, i]]);
							i++;
						});
					});

					this.db.sqlBatch(sqls).then(() => {
						// @todo: remove (dev)
						console.log('Rows inserted');

						resolve(true);
					}).catch(err => {
						// @todo: remove (dev)
						console.log('SQL batch ERROR for objects: ' + err.message);

						reject(err.message);
					});
				});
		});
	}

	setInFavourites(objectId: number, in_favourites: number) {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					return this.db.executeSql('UPDATE objects SET in_favourites = ? WHERE remote_id = ?', [in_favourites, objectId]);
				})
				.then(() => {
					resolve(true);
				})
				.catch(err => {
					// @todo: remove this (dev)
					console.log(err.message);

					reject(err.message);
				});
		});
	}

	markAsViewed(objectId: number) {
		return new Promise((resolve, reject) => {
			this.getDb()
				.then(() => {
					return this.db.executeSql('UPDATE objects SET is_viewed = ? WHERE remote_id = ?', [1, objectId]);
				})
				.then(() => {
					resolve(true);
				})
				.catch(err => {
					// @todo: remove this (dev)
					console.log(err.message);

					reject(err.message);
				});
		});
	}
}
