import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';

// System
import {Storage} from '@ionic/storage';

export interface UserInterface {
	phone: string;
	token: string;
	push: boolean;
	name?: string;
}

@Injectable()
export class AuthProvider {
	apiUrl = 'http://push-soft.com/sr/public';
	user: UserInterface;

	constructor(public http: HttpClient,
	            public storage: Storage,) {
	}

	clearConfig() {
		return new Promise(resolve => {
			this.storage.clear().then(() => {
				this.user = undefined;
				resolve();
			});
		});
	}

	getUser() {
		return new Promise((resolve, reject) => {
			if (this.user) {
				resolve(this.user);
			}

			this.storage.get('user').then(user => {
				if (user && user.phone && user.token) {
					this.user = user;
					resolve(user);
				}
				else {
					reject(false);
				}
			});
		});
	}

	storeUser(user: UserInterface) {
		return new Promise(resolve => {
			this.storage.set('user', user);
			this.user = user;
			resolve();
		});
	}


	signUp(data: Object) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apiUrl + '/signup', JSON.stringify(data), {
				headers: new HttpHeaders()
					.set('Content-Type', 'application/json')
			}).subscribe(data => {
				resolve(data);
			}, (err: HttpErrorResponse) => {
				reject(err);
			});
		});
	}

	verify(data: { phone: string, code: string }) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apiUrl + '/verify', JSON.stringify(data), {
				headers: new HttpHeaders().set('Content-Type', 'application/json')
			}).subscribe(data => {
				resolve(data)
			}, err => {
				reject(err);
			});
		});
	}
}
