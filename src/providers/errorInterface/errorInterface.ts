import i12n from '../../i12n/ru';

export interface ErrorInterface {
	status: boolean;
	error: string;
}

/**
 * Show alert with error
 * @param err
 * @param alertCtrl
 */
export function presentErrorAlert(err, alertCtrl) {
	// Get error
	let error: ErrorInterface;
	if (err && err.error && !err.message) {
		error = JSON.parse(err.error);
	}
	else {
		error = {
			status: false,
			error: typeof err === 'string' ? err : (err && err.message ? mapError(err.message) : 'Unknown error'),
		};
	}

	// @todo: remove this (dev)
	console.log(err);

	// Create alert
	let alert = alertCtrl.create({
		title: "Error",
		subTitle: error.error,
		buttons: ['OK']
	});

	// Present alert
	alert.present();
}

function mapError(error: string): string {
	if (error.indexOf('Http failure response') === 0) {
		return i12n.http_error;
	}
	return error;
}
