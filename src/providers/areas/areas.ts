import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthProvider} from "../auth/auth";
import 'rxjs/add/operator/map';

// Providers and Interfaces
import {UserInterface} from "../auth/auth";
import {DbProvider} from "../db/db";

interface AreasReturnInterfaceWrap {
	data: AreasReturnInterface[],
}

interface AreasReturnInterface {
	name: string;
	id: number;
	areas?: [{
		id: number;
		name: string;
	}];
}

export interface CityInterface {
	id: number;
	name: string;
}

export interface DistrictInterface {
	id: number;
	name: string;
	city_id: number;
}

@Injectable()
export class AreasProvider {

	apiUrl = 'http://push-soft.com/sr/public';
	cities: CityInterface[] = [];
	districts: DistrictInterface[] = [];

	constructor(public http: HttpClient,
	            public authProvider: AuthProvider,
	            public dbProvider: DbProvider,) {
	}

	getAreas() {
		// If cities already exist - resolve them
		return new Promise((resolve, reject) => {
			if (this.cities.length > 0) {
				resolve(true);
			}
			else {
				this.dbProvider.getCities()
					.then((cities: CityInterface[]) => {
						this.cities = cities;
						return this.dbProvider.getDistricts();
					})
					.then((districts: DistrictInterface[]) => {
						this.districts = districts;

						if (this.cities.length > 0) {
							return resolve(true);
						}
						else {
							// Get from API
							this.authProvider.getUser().then((user: UserInterface) => {
								this.http.get(this.apiUrl + '/getAreas', {
									headers: new HttpHeaders()
										.set('Content-Type', 'application/json')
										// @todo: change line
										.set('Authorization', 'Bearer ' + user.token)
									// .set('Authorization', 'Bearer 2cbb709daf370d62bbc6e0cec3f30934')
								}).subscribe((data: AreasReturnInterfaceWrap) => {
									data.data.forEach(item => {
										this.cities.push({
											id: item.id,
											name: item.name,
										});

										if (item.areas) {
											item.areas.forEach(district => {
												this.districts.push({
													id: district.id,
													name: district.name,
													city_id: item.id,
												});
											});
										}
									});
									resolve(true);
								}, err => {
									reject(err);
								});
							});
						}
					});
			}
		});
	}

	getCities() {
		return this.cities;
	}

	getDistricts() {
		return this.districts;
	}
}
