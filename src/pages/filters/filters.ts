import {Component} from '@angular/core';
import {Platform, NavController, NavParams, ViewController, LoadingController, AlertController} from 'ionic-angular';

// Providers
import {AreasProvider, CityInterface, DistrictInterface} from "../../providers/areas/areas";
import {ObjectsProvider, FiltersInterface} from "../../providers/objects/objects";

// Components
import {TabsPage} from "../tabs/tabs";

// Misc
import {presentErrorAlert} from "../../providers/errorInterface/errorInterface";

// i12n
import i12n from '../../i12n/ru';

@Component({
	selector: 'filters',
	templateUrl: 'filters.html',
})
export class FiltersPage {

	type: string = 'house';
	city: number;
	district: number;
	price: any = {lower: 2000, upper: 30000};

	cities: CityInterface[] = [];
	allDistricts: DistrictInterface[] = [];
	districts: DistrictInterface[] = [];
	i12n = i12n;

	constructor(public platform: Platform,
	            public params: NavParams,
	            public viewCtrl: ViewController,
	            public navCtrl: NavController,
	            public loadingCtrl: LoadingController,
	            public alertCtrl: AlertController,
	            public areasProvider: AreasProvider,
	            public objectProvider: ObjectsProvider,) {

		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// Get cities and districts
		this.areasProvider.getAreas().then(() => {
			this.cities = this.areasProvider.getCities();
			this.allDistricts = this.areasProvider.getDistricts();

			// Loading current filters
			this.objectProvider.getCurrentFilters().then((filters: FiltersInterface) => {
				this.city = filters.city_id;
				this.district = filters.city_area;
				this.price.lower = filters.price_from;
				this.price.upper = filters.price_to;

				if (this.city) {
					this.districts = this.allDistricts.filter(item => item.city_id === this.city);
				}

				// Hide loader
				loader.dismiss();
			}).catch(() => {

				// Hide loader
				loader.dismiss();
			});


		}).catch(err => {
			loader.dismiss();
			presentErrorAlert(err, this.alertCtrl);
		});


	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

	onCityChange(e) {
		this.city = e;
		this.district = 0;
		this.districts = this.allDistricts.filter(item => item.city_id === this.city);
	}

	onButtonClick() {
		if (this.city && this.price) {
			// Show loaer
			let loader = this.loadingCtrl.create({
				content: this.i12n.wait_text,
			});
			loader.present();

			// Form filters object
			let filters: FiltersInterface = {
				price_from: this.price.lower,
				price_to: this.price.upper,
				city_id: this.city,
				city_area: this.district ? this.district : null,
			};

			// Get API call
			this.objectProvider.setFilters(filters).then((data) => {
				// Hide loader
				loader.dismiss();

				// Determine if there is initial filtering
				if (!this.navCtrl.parent) {
					// If so - go to Tabs
					this.navCtrl.setRoot(TabsPage);
				}
				else {
					// Else - show success alert
					let alert = this.alertCtrl.create({
						title: this.i12n.saved,
						buttons: [this.i12n.ok],
					});

					alert.present();
				}

			}).catch(err => {
				// Hide loader
				loader.dismiss();

				// Show error
				presentErrorAlert(err, this.alertCtrl);
			});
		}
	}
}
