import {Component, OnInit} from '@angular/core';
import {Platform} from "ionic-angular";
import {NavController} from "ionic-angular";

import {ProfilePage} from '../profile/profile';
import {HomePage} from '../home/home';
import {FavPage} from '../fav/fav';
import {FiltersPage} from '../filters/filters';

// Native
import {BackgroundMode} from '@ionic-native/background-mode';
import {LocalNotifications} from '@ionic-native/local-notifications';

// Providers
import {ObjectsProvider, ObjectInterface} from "../../providers/objects/objects";
import {DbProvider} from "../../providers/db/db";
import {AuthProvider, UserInterface} from "../../providers/auth/auth";

// i12n
import i12n from '../../i12n/ru';

@Component({
	templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit {
	profileRoot = ProfilePage;
	homeRoot = HomePage;
	favRoot = FavPage;
	filtersRoot = FiltersPage;

	newObjectsCount = 0;

	i12n = i12n;

	constructor(public objectsProvider: ObjectsProvider,
	            public dbProvider: DbProvider,
	            public navCtrl: NavController,
	            private backgroundMode: BackgroundMode,
	            private localNotifications: LocalNotifications,
	            public platform: Platform,
	            public authProvider: AuthProvider,) {

		// Try to get current filters and if it don't exist - go to the filters page
		this.objectsProvider.getCurrentFilters()
			.then(() => {
				// Set background mode
				this.backgroundMode.setDefaults({silent: true});
				this.backgroundMode.enable();

				// Launch auto-updating task
				this.scheduleGetObjects(1000 * 60);
			})
			.catch(() => {
				this.navCtrl.setRoot(FiltersPage);
			});
	}

	ngOnInit() {
		this.objectsProvider.newObjectsCount.subscribe(count => this.newObjectsCount = count);
	}

	scheduleGetObjects(time: number) {
		/*let interval = */setInterval(() => {
			this.objectsProvider.getObjectsFromAPI()
				.then((objects: ObjectInterface[]) => {
					if (objects.length > 0) {

						// If push notifications is enabled
						this.authProvider.getUser().then((user: UserInterface) => {
							if (user.push) {
								this.localNotifications.schedule({
									id: 1,
									title: this.i12n.app_title,
									text: `${this.i12n.schedule_found_new} (${objects.length})`,
									sound: this.platform.is('android') ? 'file://sound.mp3' : 'file://beep.caf',
								});
							}
						});

						// Insert new objects to database
						this.dbProvider.insertObjects(objects);

						// Add new objects to the objects provider
						this.objectsProvider.addNewObjects(objects);
					}

					// Update newObjectsCount
					this.objectsProvider.changeNewObjectsCount(objects.length, 'plus');
				});
		}, time);
	}
}
