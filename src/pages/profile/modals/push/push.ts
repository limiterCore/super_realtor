import {Component} from '@angular/core';
import {ViewController, LoadingController} from 'ionic-angular';

// Providers
import {AuthProvider, UserInterface} from "../../../../providers/auth/auth";

// i12n
import i12n from '../../../../i12n/ru';

@Component({
	selector: 'push',
	templateUrl: 'push.html'
})
export class PushModal {

	user: UserInterface;
	i12n = i12n;

	constructor(public viewCtrl: ViewController,
	            public loadingCtrl: LoadingController,
	            public authProvider: AuthProvider,) {
		this.authProvider.getUser().then((user: UserInterface) => {
			this.user = user;
		});
	}

	saveChanges() {
		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// Store user
		this.authProvider.storeUser(this.user).then(() => {
			// Hide loader
			loader.dismiss();
		});
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}
}
