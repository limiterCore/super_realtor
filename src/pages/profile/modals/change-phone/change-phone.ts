import {Component} from '@angular/core';
import {ViewController, LoadingController, AlertController} from 'ionic-angular';

// Providers
import {AuthProvider, UserInterface} from "../../../../providers/auth/auth";

// i12n
import i12n from '../../../../i12n/ru';

@Component({
	selector: 'change-phone',
	templateUrl: 'change-phone.html'
})
export class ChangePhoneModal {

	user: UserInterface;
	i12n = i12n;

	constructor(public viewCtrl: ViewController,
	            public alertCtrl: AlertController,
	            public loadingCtrl: LoadingController,
	            public authProvider: AuthProvider,) {
		this.authProvider.getUser().then((user: UserInterface) => {
			this.user = user;
		});
	}

	saveChanges() {
		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// Store user
		this.authProvider.storeUser(this.user).then(() => {
			// Hide loader
			loader.dismiss();

			// Show alert
			let alert = this.alertCtrl.create({
				title: this.i12n.profile_success_message,
				buttons: [{
					text: this.i12n.profile_success_message_return,
					handler: () => {
						this.dismiss();
					},
				}],
			});
			alert.present();
		});
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}
}
