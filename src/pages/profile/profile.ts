import {Component} from '@angular/core';
import {AlertController, LoadingController, ModalController, NavController} from 'ionic-angular';
import {ChangePhoneModal} from './modals/change-phone/change-phone';
import {PushModal} from './modals/push/push';

import {RegistrationPage} from "../registration/registration";

// Providers
import {AuthProvider, UserInterface} from "../../providers/auth/auth";
import {DbProvider} from "../../providers/db/db";

// i12n
import i12n from '../../i12n/ru';
import {ObjectsProvider} from "../../providers/objects/objects";

@Component({
	selector: 'page-profile',
	templateUrl: 'profile.html'
})
export class ProfilePage {

	user: UserInterface;
	i12n = i12n;
	showErase: boolean = false;

	constructor(public modalCtrl: ModalController,
	            public authProvider: AuthProvider,
	            public dbProvider: DbProvider,
							public alertCtrl: AlertController,
							public loadingCtrl: LoadingController,
							public navCtrl: NavController,
							public objectsProvider: ObjectsProvider,) {
		this.authProvider.getUser().then((user: UserInterface) => {
			this.user = user;
		});
	}

	openChangePhoneModal() {
		let modal = this.modalCtrl.create(ChangePhoneModal);
		modal.present();
	}

	// openFiltersModal() {
	// 	let modal = this.modalCtrl.create(FiltersModal);
	// 	modal.present();
	// }

	openPushModal() {
		let modal = this.modalCtrl.create(PushModal);
		modal.present();
	}

	confirmDelete() {
		let confirm = this.alertCtrl.create({
			title: this.i12n.confirm,
			buttons: [
				{
					text: this.i12n.no,
					handler: () => {
					}
				},
				{
					text: this.i12n.yes,
					handler: () => {

						// Show loader
						let loader = this.loadingCtrl.create({
							content: this.i12n.wait_text,
						});
						loader.present();

						// API call
						this.authProvider.clearConfig()
							.then(() => {
								return this.objectsProvider.clearProperties();
							})
							.then(() => {
								return this.dbProvider.clearTables();
							})
							.then(() => {
								// Hide loader
								loader.dismiss();

								this.navCtrl.setRoot(RegistrationPage);
							})
							.catch(e => {
								// Hide loader
								loader.dismiss();
							});
					}
				}
			]
		});

		confirm.present();
	}

}
