// Core
import {Component} from '@angular/core';

// Components
import {ModalController, AlertController, LoadingController, NavController} from 'ionic-angular';
import {ActivationModal} from './modals/activation/activation';
import {TabsPage} from '../tabs/tabs';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {presentErrorAlert} from '../../providers/errorInterface/errorInterface';

// i12n
import i12n from '../../i12n/ru';

@Component({
	templateUrl: 'registration.html',
})
export class RegistrationPage {

	user = {
		name: '',
		phone: '',
	};

	i12n = i12n;

	constructor(public navCtrl: NavController,
	            public loadingCtrl: LoadingController,
	            public modalCtrl: ModalController,
	            public authProvider: AuthProvider,
	            public alertCtrl: AlertController, ) {
		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// @todo remove clearConfig wrap (dev)
		// this.authProvider.clearConfig().then(() => {
		// Try to get current user
		this.authProvider.getUser().then(() => {
			loader.dismiss();
			navCtrl.push(TabsPage);
		}).catch(() => {
			loader.dismiss();
		});
		// });
	}

	signup() {
		if (this.user.phone !== '' && this.user.name !== '') {
			// Show loader
			let loader = this.loadingCtrl.create({
				content: this.i12n.wait_text,
			});
			loader.present();

			// Interface for returned data
			interface ReturnData {
				user: string;
				status: boolean;
				message: string;
				code: string;
			}

			// Send api call
			this.authProvider.signUp(this.user)
				.then((data: ReturnData) => {
					// Hide loader
					loader.dismiss();

					// Create modal and pass parameters to it
					let modal = this.modalCtrl.create(ActivationModal, {
						activationCode: data.code,
						phone: this.user.phone,
						name: this.user.name,
						token: JSON.parse(data.user).token,
					});

					// Present modal
					modal.present();
				})
				.catch(err => {
					// Hide loader
					loader.dismiss();

					presentErrorAlert(err, this.alertCtrl);
				});
		}
	}
}
