import {Component} from '@angular/core';
import {NavController, ViewController, NavParams, AlertController, LoadingController} from 'ionic-angular';
import {FiltersPage} from '../../../filters/filters';

// Providers
import {AuthProvider} from '../../../../providers/auth/auth';
import {AreasProvider} from "../../../../providers/areas/areas";
import {DbProvider} from "../../../../providers/db/db";
import {presentErrorAlert} from "../../../../providers/errorInterface/errorInterface";

// i12n
import i12n from '../../../../i12n/ru';

@Component({
	templateUrl: 'activation.html'
})
export class ActivationModal {
	activationCode: string;
	phone: string;
	name: string;
	agree: boolean;
	i12n = i12n;

	constructor(public viewCtrl: ViewController,
	            public navCtrl: NavController,
	            public params: NavParams,
	            public authProvider: AuthProvider,
	            public alertCtrl: AlertController,
	            public loadingCtrl: LoadingController,
	            public dbProvider: DbProvider,
	            public areasProvider: AreasProvider,) {
		this.activationCode = this.params.get('activationCode');
		this.phone = this.params.get('phone');
		this.name = this.params.get('name');
		this.agree = true;
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

	activate() {
		// verify
		if (!this.activationCode) {
			let alert = this.alertCtrl.create({
				title: this.i12n.error,
				subTitle: this.i12n.activation_fill_code,
				buttons: [this.i12n.ok]
			});
			alert.present();
			return;
		}

		if (this.activationCode.length && this.activationCode.length !== 4) {
			let alert = this.alertCtrl.create({
				title: this.i12n.error,
				subTitle: this.i12n.activation_length_code,
				buttons: [this.i12n.ok]
			});
			alert.present();
			return;
		}

		// show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// 01. Send verify API call
		this.authProvider.verify({phone: this.phone, code: this.activationCode})
			.then(() => {
				return {
					phone: this.phone,
					token: this.params.get('token'),
					push: true,
					name: this.name,
				}
			})
			.then(userToStore => {
				// 02. Store user
				return this.authProvider.storeUser(userToStore);
			})
			.then(() => {
				// 03. Init tables
				return this.dbProvider.initTables();
			})
			.then(() => {
				// 04. get areas
				return this.areasProvider.getAreas();
			})
			.then(() => {
				// 05. Fill areas
				let cities = this.areasProvider.getCities();
				let districts = this.areasProvider.getDistricts();

				return this.dbProvider.fillAreas(cities, districts);
			})
			.then(() => {
				// 06. Navigate to the filters page

				// Hide loader
				loader.dismiss();
				this.navCtrl.setRoot(FiltersPage);
			})
			.catch((err) => {
				// Hide loader
				loader.dismiss();
				// Show error
				presentErrorAlert(err, this.alertCtrl);
			});
	}
}
