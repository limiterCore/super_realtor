import {Component, OnInit} from '@angular/core';
import {NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';
import {ObjectPage} from '../object/object';

// Providers
import {ObjectsProvider, ObjectInterface} from "../../providers/objects/objects";

// Misc
import {presentErrorAlert} from "../../providers/errorInterface/errorInterface";

// i12n
import i12n from '../../i12n/ru';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage implements OnInit {

	objects: ObjectInterface[] = [];
	setNumber: number = 1;
	newObjectsCount: number = 0;
	i12n = i12n;

	constructor(public navCtrl: NavController,
	            public objectsProvider: ObjectsProvider,
	            public loadingCtrl: LoadingController,
	            public alertCtrl: AlertController,
	            public params: NavParams,) {

		// Set new objects count
		this.newObjectsCount = this.params.get('newObjectsCount');
	}

	ngOnInit() {
		this.objectsProvider.newObjectsCount.subscribe(count => this.newObjectsCount = count);

		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// Get objects
		this.objectsProvider.getObjects().then(() => {
			loader.dismiss();
			this.objectsProvider.objects.subscribe(objects => this.objects = objects);
		}).catch(err => {
			loader.dismiss();
			presentErrorAlert(err, this.alertCtrl);
		});
	}

	openObject(id: number) {
		this.navCtrl.push(ObjectPage, {objectId: id});
	}

	addToFavorites(id: number) {
		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// API call
		this.objectsProvider.addToFavourites(id).then(() => {
			// Hide loader
			loader.dismiss();
		}).catch(e => {
			// Hide loader
			loader.dismiss();

			// Show alert
			let alert = this.alertCtrl.create({
				title: this.i12n.error,
				subTitle: this.i12n.object_not_found,
				buttons: [this.i12n.ok],
			});
			alert.present();
		});
	}

	markAsViewed(event, id: number, is_viewed: number) {
		if (is_viewed) {
			return false;
		}
		if (event.value) {
			// API call
			this.objectsProvider.markAsViewed(id)
				.then(() => {
					setTimeout(() => {
						this.objectsProvider.changeNewObjectsCount(1, 'minus');
					}, 1);
				})
				.catch(error => {
					presentErrorAlert(error, this.alertCtrl);
				});
		}
	}

	loadRecentItems(infiniteScroll) {
		this.objectsProvider.getMoreObjects(this.setNumber)
			.then(() => {
				this.setNumber ++;
				infiniteScroll.complete();
			})
			.catch(() => {
				infiniteScroll.complete();
				infiniteScroll.enable(false);
			});

	}

}
