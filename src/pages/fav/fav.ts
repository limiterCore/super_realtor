import {Component, OnInit} from '@angular/core';
import {NavController, AlertController, LoadingController} from 'ionic-angular';
import {ObjectPage} from '../object/object';

// Providers
import {ObjectsProvider, ObjectInterface} from "../../providers/objects/objects";

// Misc
import {presentErrorAlert} from "../../providers/errorInterface/errorInterface";

// i12n
import i12n from '../../i12n/ru';

@Component({
	selector: 'page-fav',
	templateUrl: 'fav.html'
})
export class FavPage implements OnInit {

	objects: ObjectInterface[];
	i12n = i12n;
	setNumber: number = 1;

	constructor(public navCtrl: NavController,
	            public alertCtrl: AlertController,
	            public loadingCtrl: LoadingController,
	            public objectsProvider: ObjectsProvider,) {

	}

	ngOnInit() {
		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// Get objects
		this.objectsProvider.getFavObjects().then((objects: ObjectInterface[]) => {
			loader.dismiss();
			this.objectsProvider.objectsFav.subscribe(objects => this.objects = objects);
		}).catch(err => {
			loader.dismiss();
			presentErrorAlert(err, this.alertCtrl);
		});
	}

	removeFromFavourites(id: number) {
		let confirm = this.alertCtrl.create({
			title: this.i12n.object_remove_fav,
			buttons: [
				{
					text: this.i12n.no,
					handler: () => {
					}
				},
				{
					text: this.i12n.yes,
					handler: () => {

						// Show loader
						let loader = this.loadingCtrl.create({
							content: this.i12n.wait_text,
						});
						loader.present();

						// API call
						this.objectsProvider.removeFromFavourites(id).then(() => {
							// Hide loader
							loader.dismiss();
						}).catch(e => {
							// Hide loader
							loader.dismiss();

							// Show alert
							let alert = this.alertCtrl.create({
								title: this.i12n.error,
								subTitle: this.i12n.object_not_found,
								buttons: [this.i12n.ok],
							});
							alert.present();
						});
					}
				}
			]
		});

		confirm.present();
	}

	openObject(id: number) {
		this.navCtrl.push(ObjectPage, {objectId: id});
	}

	loadRecentItems(infiniteScroll) {
		this.objectsProvider.getMoreFavObjects(this.setNumber)
			.then(() => {
				this.setNumber ++;
				infiniteScroll.complete();
			})
			.catch(() => {
				infiniteScroll.complete();
				infiniteScroll.enable(false);
			});
	}

}
