import {Component} from '@angular/core';
import {NavController, AlertController, NavParams, LoadingController} from 'ionic-angular';
import {CallNumber} from '@ionic-native/call-number';

// Providers
import {ObjectsProvider, ObjectInterface} from "../../providers/objects/objects";

// i12n
import i12n from '../../i12n/ru';

@Component({
	selector: 'page-object',
	templateUrl: 'object.html'
})
export class ObjectPage {

	objects: ObjectInterface[];
	objectsFav: ObjectInterface[];
	item: ObjectInterface;
	i12n = i12n;

	constructor(public navCtrl: NavController,
	            public alertCtrl: AlertController,
	            public loadingCtrl: LoadingController,
	            private callNumber: CallNumber,
	            public objectsProvider: ObjectsProvider,
	            public params: NavParams,) {

		// Try to find in objects
		this.objectsProvider.objects.subscribe(items => {
			this.objects = items;
			let filteredItems = this.objects.filter(item => item.id === this.params.get('objectId'));

			if (!filteredItems.length) {

				// Try to find in favourites
				this.objectsProvider.objectsFav.subscribe(items => {
					this.objectsFav = items;
					filteredItems = this.objectsFav.filter(item => item.id === this.params.get('objectId'));
				});
			}
			this.item = filteredItems[0];
		});
	}

	back() {
		this.navCtrl.pop();
	}

	callNumberFunc(tel: string) {
		this.callNumber.callNumber(tel, true)
			.then(() => {
				// @todo: remove this (dev)
				console.log('Launched dialer!')
			})
			.catch(e => {
				// @todo: remove this (dev)
				console.log('Error launching dialer', e)
			});
	}

	addToFavorites(id: number) {

		// Show loader
		let loader = this.loadingCtrl.create({
			content: this.i12n.wait_text,
		});
		loader.present();

		// API call
		this.objectsProvider.addToFavourites(id).then(() => {
			// Update this.objects
			// let items = objects.filter(item => item.id === this.params.get('objectId'));
			// this.item = items[0];

			// Hide loader
			loader.dismiss();
		}).catch(e => {
			// Hide loader
			loader.dismiss();

			// Show alert
			let alert = this.alertCtrl.create({
				title: this.i12n.error,
				subTitle: this.i12n.object_not_found,
				buttons: [this.i12n.ok],
			});
			alert.present();
		});
	}
}
