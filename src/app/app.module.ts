import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {IonicStorageModule} from '@ionic/storage';
import {MyApp} from './app.component';

import {CallNumber} from '@ionic-native/call-number';
import {SQLite} from "@ionic-native/sqlite";
import {BackgroundMode} from "@ionic-native/background-mode";
import {LocalNotifications} from "@ionic-native/local-notifications"

import {RegistrationPage} from '../pages/registration/registration';
import {ProfilePage} from '../pages/profile/profile';
import {HomePage} from '../pages/home/home';
import {FavPage} from '../pages/fav/fav';
import {ObjectPage} from '../pages/object/object';
import {TabsPage} from '../pages/tabs/tabs';

import {ChangePhoneModal} from '../pages/profile/modals/change-phone/change-phone';
import {FiltersPage} from '../pages/filters/filters';
import {PushModal} from '../pages/profile/modals/push/push';
import {ActivationModal} from '../pages/registration/modals/activation/activation';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthProvider} from '../providers/auth/auth';
import {DbProvider} from '../providers/db/db';
import {AreasProvider} from '../providers/areas/areas';
import {ObjectsProvider} from '../providers/objects/objects';

import {InViewportModule} from 'ng-in-viewport';

import 'intersection-observer';


@NgModule({
	declarations: [
		MyApp,
		RegistrationPage,
		ProfilePage,
		HomePage,
		FavPage,
		ObjectPage,
		TabsPage,
		ChangePhoneModal,
		FiltersPage,
		PushModal,
		ActivationModal,
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		IonicModule.forRoot(MyApp),
		IonicStorageModule.forRoot(),
		InViewportModule.forRoot(),
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		RegistrationPage,
		ProfilePage,
		HomePage,
		FavPage,
		ObjectPage,
		TabsPage,
		ChangePhoneModal,
		FiltersPage,
		PushModal,
		ActivationModal,
	],
	providers: [
		StatusBar,
		SplashScreen,
		{provide: ErrorHandler, useClass: IonicErrorHandler},
		CallNumber,
		AuthProvider,
		DbProvider,
		AreasProvider,
		// {provide: SQLite, useClass: SQLiteMock},
		{provide: SQLite, useClass: SQLite},
		ObjectsProvider,
		BackgroundMode,
		LocalNotifications,
	]
})
export class AppModule {
}
